package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"restapplication"
	"time"
)

var StartTime = time.Now()										// Sets the time of startup

func main() {

	restapplication.StartTime = StartTime						// Gives the start time to the variable in Handler.go

	port := os.Getenv("PORT")								// Gets the url from Heroku
	if port == ""{
		log.Fatal("PORT must be specified")
	}

	http.HandleFunc("/conservation/v1/country/", restapplication.CountryHandler)
	http.HandleFunc("/conservation/v1/species/", restapplication.SpeciesHandler)
	http.HandleFunc("/conservation/v1/diag/", restapplication.DiagHandler)
	http.HandleFunc("/", restapplication.HomepageHandler)

	fmt.Println("Listening on port " + port)
	listenError := http.ListenAndServe(":" + port, nil)
	if listenError != nil{
		fmt.Println(fmt.Errorf("Error while Listening and serving on port " + port + "\n"))
	}
}