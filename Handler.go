package restapplication

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var StartTime time.Time
var Diags[] Diag
const COUNTRYURL = "https://restcountries.eu/rest/v2/alpha/"
const SPECIESURL = "http://api.gbif.org/v1/species/"


//Function first calls a http.get request for the country specified by the user, then decodes it to the Country struct.
//Then a http.get request for occurrences is called, this includes most information about multiple species
// and is decodes to the country struct, who contains an array of occurrences objects.
//Finally a http.get request for the speceiskeys are called and decoded to the AllSpeciesKeys struct,
// which contains an array of SpeciesKey objects.
//The array of SpeciesKeys in country struct is then set to be the whole array of specieskeys in AllSpeciesKeys.
func CountryHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet{
		http.Header.Add(writer.Header(), "content-type", "application/json")

		//Splits the URL specified by the user, everywhere there is a "/".
		URLParts := strings.Split(r.URL.Path, "/")


		if URLParts[1] == "conservation" && URLParts[2] == "v1" && URLParts[3] == "country" && len(URLParts[4]) == 2{

			//Converts the string to a int number.
			numberOfSpecies, convertError := strconv.Atoi(URLParts[5])
			CheckError(convertError, writer)

			occurrenceURL := "http://api.gbif.org/v1/occurrence/search?country=" + URLParts[4] + "&limit=" + URLParts[5]

			var country = &Country{}

			countryResponse, getError := http.Get(COUNTRYURL + URLParts[4])
			CheckError(getError, writer)

			if countryResponse.StatusCode != 200{
				var d = Diag{}
				d.RestCountries = countryResponse.StatusCode
				d.Version = URLParts[2]
				d.Uptime = int(Uptime()/1000000000) // Converts value from Uptime() to and int, and removes the nine decimals
				Diags = append(Diags, d)
			}

			decodeErr := json.NewDecoder(countryResponse.Body).Decode(country)
			CheckError(decodeErr, writer)

			//Closes the body to lessen the chance of errors.
			closeErr := countryResponse.Body.Close()
			CheckError(closeErr, writer)

			occurrenceResponse, getError := http.Get(occurrenceURL)
			CheckError(getError, writer)

			if occurrenceResponse.StatusCode != 200 {
				var d= Diag{}
				d.Gbif = occurrenceResponse.StatusCode
				d.Version = URLParts[2]
				d.Uptime = int(Uptime()/1000000000)
				Diags = append(Diags, d)
			}

			decodeErr = json.NewDecoder(occurrenceResponse.Body).Decode(country)
			CheckError(decodeErr, writer)

			//Closes the body to lessen the chance of errors.
			closeErr = occurrenceResponse.Body.Close()
			CheckError(closeErr, writer)

			occurrenceKeyResponse, getError := http.Get(occurrenceURL)
			CheckError(getError, writer)

			if occurrenceKeyResponse.StatusCode != 200 {
				var d= Diag{}
				d.Gbif = occurrenceResponse.StatusCode
				d.Version = URLParts[2]
				d.Uptime = int(Uptime())
				Diags = append(Diags, d)
			}

			var allSpeciesKey= &AllSpeciesKey{}
			decodeErr = json.NewDecoder(occurrenceKeyResponse.Body).Decode(allSpeciesKey)
			CheckError(decodeErr, writer)

			//Creates an int array with same length of numberOfSpecies.
			speciesKeys := make([]int, numberOfSpecies)

			for i := 0; i < numberOfSpecies; i++ {
				speciesKeys[i] = allSpeciesKey.SpeciesKey[i].SpeciesKey
			}

			country.SpeciesKey = speciesKeys

			//Closes the body to lessen the chance of errors.
			closeErr = occurrenceKeyResponse.Body.Close()
			CheckError(closeErr, writer)

			//Closes the body to lessen the chance of errors.
			closeErr = countryResponse.Body.Close()
			CheckError(closeErr, writer)

			encodeErr := json.NewEncoder(writer).Encode(country)
			CheckError(encodeErr, writer)

		} else { http.Error(writer, "Malformed URL", http.StatusBadRequest) }
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}


//First a http.get request for species is called and decoded to the species struct
//Then a second http.get request for the year for the species is called, and decoded to the same species struct.
func SpeciesHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet{
		http.Header.Add(writer.Header(), "content-type", "application/json")

		//Splits the URL specified by the user, everywhere there is a "/".
		URLParts := strings.Split(r.URL.Path, "/")

		if len(URLParts) == 5 && URLParts[1] == "conservation" && URLParts[2] == "v1" && URLParts[3] == "species"{
			var species = &Species{}

			response, getError := http.Get(SPECIESURL + URLParts[4])
			CheckError(getError, writer)

			if response.StatusCode != 200{
				var d = Diag{}
				d.Gbif = response.StatusCode
				d.Version = URLParts[2]
				d.Uptime = int(Uptime()/1000000000)
				Diags = append(Diags, d)
			}

			decodeErr := json.NewDecoder(response.Body).Decode(species)
			CheckError(decodeErr, writer)

			//Closes the body to lessen the chance of errors.
			closeErr := response.Body.Close()
			CheckError(closeErr, writer)

			response, getError = http.Get(SPECIESURL + URLParts[4] + "/name")
			CheckError(getError, writer)

			if response.StatusCode != 200{
				var d = Diag{}
				d.Gbif = response.StatusCode
				d.Version = URLParts[2]
				d.Uptime = int(Uptime()/1000000000)
				Diags = append(Diags, d)
			}

			decodeErr = json.NewDecoder(response.Body).Decode(species)
			CheckError(decodeErr, writer)

			//Closes the body to lessen the chance of errors.
			closeErr = response.Body.Close()
			CheckError(closeErr, writer)

			encodeErr := json.NewEncoder(writer).Encode(species)
			CheckError(encodeErr, writer)

		} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}


// Function which displays all diagnostic messages since startup
func DiagHandler(writer http.ResponseWriter, r*http.Request){
	if r.Method == http.MethodGet{
		http.Header.Add(writer.Header(), "content-type", "application/json")
		if r.URL.Path == "/conservation/v1/diag/"{								// Makes sure the intended URL is written
			var dia = &Diagnostics{}											// Creates a temporary Diagnostics object
			diagArray := make([]Diag, len(Diags))								// Creates a temporary array that shall contain all diagnostic messages
			for i := range Diags{												// Fills the array with the occured diagnostics
				diagArray[i] = Diags[i]
			}
			dia.Diagnostics = diagArray											// Puts the temporary array into the struct objects array
			encodeErr := json.NewEncoder(writer).Encode(dia)					// Encodes the array in the struct so it is displayed on the web page
			CheckError(encodeErr, writer)
		} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
	} else { http.Error(writer, "not implemented yet", http.StatusNotImplemented) }
}

//Function for homepage
func HomepageHandler(writer http.ResponseWriter, r*http.Request){
	if r.URL.Path == "/" {
		bytes, printError := fmt.Fprintf(writer, "What can you do on this page?\n\n"+
			" 1. You can get information on a number of animal species from a specific country using:\n"+
			"/conservation/v1/country/*COUNTRYCODE*/*NUMBER OF SPECIES*\n\n"+
			"2. You can get information on a specific animal species using:\n"+
			"/conservation/v1/species/*SPECIES ID*\n\n"+
			"3. To display potentional errors during runtime, use:\n"+
			"/conservation/v1/diag")

		CheckError(printError, writer)
		if bytes == 0{
			fmt.Println(fmt.Errorf("No text was written to page\n"))
		}

	} else {http.Error(writer, "Malformed URL", http.StatusBadRequest)}
}

// Returns the uptime for the program
func Uptime() time.Duration {
	return time.Since(StartTime)
}

//Writes out an Internal Server Error if the error parameter is not nil.
func CheckError(error error, writer http.ResponseWriter){
	if error != nil{
		http.Error(writer, error.Error(), http.StatusInternalServerError)
	}
}