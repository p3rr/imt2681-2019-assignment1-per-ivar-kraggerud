package restapplication


type Country struct{								// A struct which contains information about a country
	Code string `json:"alpha2code"`
	ContryName string `json:"name"`
	CountryFlag string `json:"flag"`
	Species []Occurrence `json:"results"`
	SpeciesKey []int
}

type AllSpeciesKey struct{		//A struct to containt all SpeciesKey objects.
	SpeciesKey []SpeciesKey `json:"results"`
}

type SpeciesKey struct{			//A struct to contain one speciesKey.
	SpeciesKey int `json:"speciesKey"`
}

type Occurrence struct{			//A struct with information about a species from one Occurrence.
	Kingdom string `json:"kingdom"`
	Phylum string `json:"phylum"`
	Order string `json:"order"`
	Family string `json:"family"`
	Genus string `json:"genus"`
	ScientificName string `json:"scientificName"`
}

type Species struct{								// A struct which contains information about an animal species
	Key int `json:"nubKey"`
	Kingdom string `json:"kingdom"`
	Phylum string `json:"phylum"`
	Order string `json:"order"`
	Family string `json:"family"`
	Genus string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName string `json:"canonicalName"`
	Year string `json:"year"`
}

type Diag struct{									// A struct which contains the diagnostics reports
	Gbif int `json:"gbif"`
	RestCountries int `json:"restcountries"`
	Version string `json:"version"`
	Uptime int `json:"uptime"`
}

type Diagnostics struct{							// A struct which is filled with all diag reports before they are
													// displayed by the diag handler.
	Diagnostics []Diag
}